---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Refactoring with Composition API
@snapend

```js
import Modal from "@/components/Modal";
export default {
  data: () => ({
   modalOpen: true  
  }),
  methods: {
   toggleModalState() { this.modalOpen = !this.modalOpen; }
  },
  components: { Modal }
};
```

Note:

Let's now focus on the script section of our root component.

All the logic here is really for one thing - to manage the state of the modal.

In order to make this code resusable in other parts of the app or perhaps even in different project, we should abstract this to a seperate file.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Refactoring with Composition API
@snapend

*src/App.vue*

```js
...
import { ref } from "vue";
export default {
  setup () {
    const modalOpen = ref(false);
    const toggleModalState = () => {
      modalOpen.value = !modalOpen.value;
    };
    return { modalOpen, toggleModalState }
  },
  ...
};
```

@[4-10]
@[2,5]
@[6-8]
@[9]

Note:

To do this, we'll refactor the component to use the Composition API. Let's go through the steps of how I got to this.

#### 4-10

Components using the Composition API will define the functionality in the `setup` method which will replace the `data`, `methods`, and other options.

Since this is just a refactor, we still have the same functionality as before, we've just changed how we create it.

#### 2,5

So, firstly, our data property `modalOpen`. One way you can create reactive variables using the Composition API by using the `ref` API method. 

#### 6-8

The `toggleModalState` method is just a plain JavaScript function. However, notice that to change the value of `modalOpen` we need to change its sub-property `value`. 

That's because reactive variables created using `ref` are wrapped in an object in order to retain their reactivity as they're passed around. 

#### 9

Finally, we return `modalOpen` and `toggleModalState` from the `setup` method in order to make them accessible to the template when it's rendered.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Refactoring with Composition API
@snapend

*terminal*

```bash
$ touch src/components/useModalState.js
```

Note:

The next step to make the code reusable is to move the functionality into a separate JavaScript module.

So let's create a module file `useModalState.js`.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Refactoring with Composition API
@snapend

*src/components/useModalState.js*

```js
import { ref } from "vue";
export default function () {
  const modalOpen = ref(false);
  const toggleModalState = () => {
    modalOpen.value = !modalOpen.value;
  };
  return { modalOpen, toggleModalState }
};
```

Note:

We can now move all of the contents of the setup function and it's dependencies into this module.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Refactoring with Composition API
@snapend

*src/App.vue*

```js
import Modal from "@/components/Modal.vue";
import useModalState from "@/components/useModalState";
export default {
  setup () {
    const { modalOpen, toggleModalState } = useModalState();
    return {
      modalOpen,
      toggleModalState
    }
  },
  ...
};
```

@[2]
@[5-9]
@[1-12]

Note:

#### 2

Back in App.vue, we'll import the `useModalState` module we just created.

#### 5-9

And then, we'll grab the state and method from that module and make them available to our root component template.

#### 1-15

And now we've successfully abstracted the functionality for managing the modal state into a module file that can easily be reused anywhere in the app.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Reason for change?
@snapend

*src/App.vue*

```html
<template>
  <button @click="toggleModalState">Open modal</button>
  <modal v-if="modalOpen">
    <p>Hello, I'm a Vue 3 modal window.</p>
  </modal>
</template>
<script>
import modal from "@/mixins/modal";
export default {
  mixins: [modal]
}
</script>
```

@[7-12]
@[1-12]

Note:

To make this feature reusable in Vue 2 we would have used mixins. These are still avaialble in Vue 3 but are essentially obsolete due to the composition API. 

#### 8,10

If we did create a mixin instead of using the composition API, we would move any relevant functionality into a separate module file which we could import and then assign to our component.

#### 1-12

One reason that mixins aren't a great way to reuse code is that just by looking at your component, it's not easy to link the render context to the component definition.

What I mean is, we're using these variables `toggleModalState` and `modalOpen` in the template, but we don't see them in the component definition because they've been abstracted into the mixin.

In a simple example like this, it doesn't seem so bad, but but if we used several mixins for this component, and if the component was much bigger, say, 50 or more lines, suddenly it would be quite a challenge to keep track of what variable comes from where and this would make it much harder to reason about and to maintain.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Refactoring with Composition API
@snapend

*src/App.vue*

```js
import Modal from "@/components/Modal.vue";
import useModalState from "@/components/useModalState";
export default {
  setup () {
    const { modalOpen, toggleModalState } = useModalState();
    return {
      modalOpen,
      toggleModalState
    }
  },
  ...
};
```

@[2,5]

Note:

#### 2,5

Compare this to the composition API where we can clearly see where our features are coming from and we have the flexibility of JavaScript modules which allows us to name imports as we want. 