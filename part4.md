---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Teleporting content
@snapend

*public/index.html*

```html
<body>
  <div>
    <!--main page content here-->
  </div>
  <!--modal here-->
</body>
``` 

@[5]

Note:

Let's change the topic now and look at another new feature of Vue 3 called `teleport`.

#### 5

If you've ever created a modal window feature before, you'll know that it's commonly positioned just before the closing `</body>` tag in the document.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[midpoint span-100 text-center]
![](assets/img/vue_3_course_v1_modal.png)
@snapend

Note:

This is done because modals usually have a page-covering background.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Teleporting content
@snapend

*public/index.html*

```html
<body>
  <div>
    <!--main page content here-->
  </div>
  <!--modal here-->
</body>
``` 

Note:

To implement a page-covering background with CSS, you don't want to have to deal with parent elements positioning and z-index stacking context, and so the simplest solution is to put the modal at the very bottom of the DOM.

This creates an issue with Vue, though, which assumes the UI will be built as a single tree of components. To allow segments of the tree to be moved elsewhere in the DOM, the new `teleport` component has been added in Vue 3. 

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Teleporting content
@snapend

*public/index.html*

```html
<body>
  ...
  <div id="app"></div><!--Vue mounting element-->
  <div id="modal-wrapper">
    <!--modal should get moved here-->
  </div>
</body>
```

@[4-6]

Note:

To use the teleport, let's first add an element to the page where we want our modal content to be moved to. 

#### 4-6

So in the `index.html` file we'll place a `div` with ID `modal-wrapper` adjacent to Vue's mounting element.

In Vue 2, this element would of course be inaccessible from the Vue instance because it falls outside of the Vue template.   

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Teleporting content
@snapend

*src/App.vue*

```html
<template>
  <button @click="toggleModalState">Open modal</button>
  <teleport to="#modal-wrapper">
    <modal v-if="modalOpen">
      <p>Hello, I'm a Vue 3 modal window.</p>
    </modal>
  </teleport>
</template>
<script>
...
</script>
```

@[3,7]
@[4-6]

Note:

##### 3,7

Now, back in `App.vue`, we're going to wrap the modal content in the `teleport` component. We'll also need to specify a `to` attribute which will be assigned a query selector identifying the target element, in this case, `#modal-wrapper`.

#### 4-6

Now, any content within the `teleport` will be rendered within the target element. However, it will still function like it was in it's original position in the hierarchy (regarding props, events etc).

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Rendered output
@snapend

```html
<body>
  ...
  <div id="app">...</div>
  <div id="modal-wrapper">
    <div class="modal">
      <p>Hello, I'm a Vue 3 modal window.</p>
    </div>
  </div>
</body>
```

Note:

Once you open the modal and inspect the state of the DOM, you'll see the modal is inside `modal-wrapper` element which is of course adjacent to the Vue mounting element. Something that was not possible in Vue 2 without a plugin.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Emitting events in Vue 3
@snapend

*src/components/Modal.vue*

```html
<template>
  <div class="modal">
    <slot></slot>
    <button @click="$emit('close')">Dismiss</button>
  </div>
</template>
```

@[4]

Note:

Coming now to the modal component, 

#### 4

Let's now add a button allowing it to be closed. To do this, we're going to add a `button` element to the modal template with a click handler that emits an event `close`.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Emitting events in Vue 3
@snapend

*src/App.vue*

```html
<template>
  ...
    <modal 
      v-if="modalOpen" 
      @close="toggleModalState"
    >
      <p>Hello, I'm a Vue 3 modal window.</p>
    </modal>
  </teleport>
</template>
```

@[4-5]

Note:

In our root App component, 

#### 4-5

We'll capture this event and cause it to trigger `toggleModalState` which will toggle the value of `modalOpen`, logically making it `false` and causing the window to close.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Emitting events in Vue 3
@snapend

*src/components/Modal.vue*

```js
export default {
  emits: [ "close" ]
}
```

Note:

So far, this feature is identical as it would be in Vue 2. However, in Vue 3 it's now recommended that you explicitly state a component's events using the new `emits` component option. 

Just like with props, you can simply create an array of strings to name each event the component will emit.

Why? Well, Imagine opening the file of a component that someone else wrote, and seeing its props and events explicitly declared. Immediately, you would understand the interface of this component i.e. what it's meant to send and receive.

In addition to providing self-documenting code, you can also use the events declaration to validate your event payload, though I couldn't find a reason to do that in this example.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Styling slot content in Vue 3
@snapend

*src/App.vue*

```html
<template>
  <button @click="toggleModalState">Open modal</button>
  <teleport to="#modal-wrapper">
    <modal v-if="modalOpen">
      <p>Hello, I'm a Vue 3 modal window.</p>
    </modal>
  </teleport>
</template>
```

@[4-6]

Note:

To make our modal reusable, we've provided a slot for content. 

#### 4-6

In the App component we've put a paragraph in the slot for content.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Styling slot content in Vue 3
@snapend

*src/components/Modal.vue*

```html
<template>
  <div class="modal">
    <slot></slot>
    <button @click="$emit('close')">Dismiss</button>
  </div>
</template>
<script>...</script>
<style scoped>
  p {
    font-style: italic;
  }
</style>
```

@[8]
@[8-12]

Note:

Back in the modal component,

#### 8

Let's begin to style that content by adding a `style` tag to the component.

It's a good practice to use `scoped` CSS in our components to ensure the rules we provide don't have unintended effects on other content in the page.

#### 8-12

Let's make it so any paragraph text that gets put into the slot is italic. To do this, we'll create a new CSS rule using the `p` selector.

If you try this, though, you'll find that it doesn't work. The issue is that scoped styling is determined at compile time when the slot content still belongs to the parent.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Styling slot content in Vue 3
@snapend

*src/components/Modal.vue*

```html
<style scoped>
  ::v-slotted(p) {
    font-style: italic;
  }
</style>
```

@[1-5]

Note:

#### 1-5

The solution provided by Vue 3 is to provide a pseudo selector `::v-slotted()` allowing you to target slot content with scoped rules in the component providing the slot. 

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[midpoint span-100 text-center]
![](assets/img/vue_3_course_v1_modal.png)
@snapend

Note:

Using ::v-slotted will apply the italic effect to the paragraph content in the slot now.

nd by the way, Vue 3 also includes some other new scoped styling selectors `::v-deep` and `::v-global`.

So that covers all the features of Vue 3 I've got time to test-drive today.

There are still quite a few that I didn't get to cover, but I'm sure some of the other talks will cover these features and you can, of course, look at the Vue 3 docs.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Get the slides and demo project
@snapend

@snap[midpoint span-100 text-center]
## https://vuejsdevelopers.com/vg2020
@snapend

Note:

Anyway, if you'd like to get the slides or the demo project I was using you can visit this page https vuejsdevelopers dot com slash vg2020 as in "Vue Global 2020" and those will be linked there.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

@snap[north span-100]
### Learn Vue 3 hands on in my video courses
@snapend

@snap[west span-50 text-center]
![](assets/img/first-vue-app.png)
@snapend
@snap[east span-50 text-center]
![](assets/img/spa-router-cli.png)
@snapend

@snap[south span-100]
## Available at https://vuejsdevelopers.com/vg2020
@snapend

Note:

And finally if you'd like to learn Vue 3 in a video course, then you may like to check out these two courses on vuejsdevelopers.com. 

These are my two introductory Vue courses, "Build Your First Vue App" and "Build a Single-Page App with Vue Router and Vue CLI".

I've just updated both of these to use Vue 3 so you can check those out later if you're interested. You can find both of them on the same page, vuejsdevelopers dot com slash vg2020.

---?color=linear-gradient(277deg, #222527 300px, #1d1f21 0)

## Thanks!
#### P.S. follow me @anthonygore and @vuejsdevelopers

Note:

So with that I'll wrap up my talk. Thanks for watching it and please enjoy the rest of the conference.

Bye for now!